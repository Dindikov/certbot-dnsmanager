#!/bin/bash
#

_dir="$(dirname "$0")"

source "$_dir/config.sh"

AUTH_ID=$(curl -s -X GET "https://$API_HOST/dnsmgr?out=json&func=auth&username=$API_USERNAME&password=$API_PASSWORD" \
| jq '.doc.auth."$id"'|tr -d \")

if [[ "$AUTH_ID" == "null" ]]; then echo "exit1" ; exit 1 ; fi

echo "AUTH_ID:"$AUTH_ID >> $LOGFILE_AU

DOMAIN=$(echo $CERTBOT_DOMAIN|cut -d . -f 2-)
echo "DOMAIN="$DOMAIN

# Create TXT record
CREATE_DOMAIN="_acme-challenge.$CERTBOT_DOMAIN."

OUT=$(curl -s -X GET "https://$API_HOST/dnsmgr?out=json&auth=$AUTH_ID&func=domain.record.edit&name=$CREATE_DOMAIN&plid=$DOMAIN&rtype=txt&ttl=3600&value=$CERTBOT_VALIDATION&sok=ok")

#echo "OUT="$OUT >> $LOGFILE_AU

for tsleep in 15 30 60; do
  echo "sleep "$tsleep
  sleep $tsleep
  if dig +short TXT $CREATE_DOMAIN |grep -q $CERTBOT_VALIDATION ;
  then
    exit 0
  fi
done
